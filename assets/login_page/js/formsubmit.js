 $(document).ready(function () {

     $("#signupForm").validate({
         ignore: ":hidden",
         rules: {
             user_first_name: {
                 required: true,
                 minlength: 3
             },
             user_last_name: {
                 required: true,
                 minlength: 3
             },
             user_password: {
		required: true,
		minlength: 8
             },
             re_password: {
		required: true,
		minlength: 8,
		equalTo: "#user_password"
             },
            user_email: {
		required: true,
                minlength: 8,
		email: true
            }           
         },
submitHandler: function (form) {
             alert('valid form submission'); // for demo
    $.ajax({
    type: "POST",
    url: "<?php echo base_url('registration/sign_up'); ?>",
    data: $(form).serialize(),
    cache: false,
       success: function(data) {
           var obj = $.parseJSON(data);
                if(obj!=null)
                            {   
                                $('#fname_err').text("");                            
                                $('#fname_err').html(obj['user_first_name']);
                                $('#lname_err').text("");
                                $('#lname_err').html(obj['user_last_name']);
                                $('#pass_err').text("");
                                $('#pass_err').html(obj['user_password']);
                                $('#repass_err').text("");
                                $('#repass_err').html(obj['re_password']);
                                $('#email_err').text("");
                                $('#email_err').html(obj['user_email']);
                                $('#gender_err').text("");
                                $('#gender_err').html(obj['user_gender']);                                
                                $('#agree_err').text("");
                                $('#agree_err').html(obj['agree']);                                
                            }
                            else
                            {                               
                                $('#fname_err').text("");
                                $('#fname_err').html(obj);
                                $('#lname_err').text("");
                                $('#lname_err').html(obj);
                                $('#pass_err').text("");
                                $('#pass_err').html(obj);
                                $('#repass_err').text("");
                                $('#repass_err').html(obj);
                                $('#email_err').text("");
                                $('#email_err').html(obj);
                                $('#gender_err').text("");
                                $('#gender_err').html(obj);
                                $('#agree_err').text("");
                                $('#agree_err').html(obj);                                
                            }
         }
            });
        return false; // required to block normal submit since you used ajax
         }

     });

 });