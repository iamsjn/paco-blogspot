<?php
/**
 * Description of Blog Model
 *
 * @author Khairul Islam
 */
class Blog_post_model extends CI_Model {
    
    public function __construct() 
    {
        parent::__construct();
        $this->load->database();
    }  
        
    function save_post($data)
    {
        $this->db->set('date', 'NOW()', FALSE);
	$this->db->insert('user_blog_post', $data);
        return true;
    }
    
    function save_comment($data)
    {
        $this->db->set('date', 'NOW()', FALSE);
	$this->db->insert('user_blog_post_comment', $data);
        return true;
    }
    
 function get_comment($u_id, $p_id)
 {
        $this->db->select('commenter_id, comment, date');
        $this->db->from('user_blog_post_comment');
        $this->db->where('user_id', $u_id);
        $this->db->where('post_id', $p_id);
        $query = $this->db->get();
        
        return $query->result();
 }    
    
 function get_post($id)
 {
        $this->db->select('id, blog_post_title, blog_post_content');
        $this->db->from('user_blog_post');
        $this->db->where('user_id', $id);
        $query = $this->db->get();
        
        return $query->result();
 }
 
 function get_post_2($id)
 {
        $this->db->select('id, blog_post_title, blog_post_content');
        $this->db->from('user_blog_post');
        $this->db->where('id', $id);
        $query = $this->db->get();
        
        return $query->result();
 }  
    
    function update_blog($data, $id)
    {
        $this->db->where('user_id', $id);
        $this->db->update('user_blog', $data); 
        return true;
    }    
    
   
 function get_blog_data($data)
 {
        $this->db->select('blog_title, blog_sub_title');
        $this->db->from('user_blog');
        $this->db->where('user_id', $data);
        $this->db->limit(1);
        $query = $this->db->get();

        if($query -> num_rows() >= 1)
        {
          return $query->result();
        }
        else
        {
          return false;
        }
 }  
    
}
