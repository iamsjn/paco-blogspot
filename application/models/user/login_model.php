<?php
/**
 * Description of login &logout
 *
 * @author Khairul Islam
 */
Class Login_model extends CI_Model
{
    public function __construct() 
    {
        parent::__construct();
        $this->load->database();
    }
    
 function check_user_data($user_name, $user_password)
 {
        $this->db->select('id, user_name, user_first_name, user_last_name, user_email, user_photo, user_gender, agree');
        $this->db->from('user_info');
        $this->db->where('user_name', $user_name);
        $this->db->where('user_password', MD5($user_password));
        $this->db->limit(1);

        $query = $this->db->get();

        if($query -> num_rows() == 1)
        {
          return $query->result();
        }
        else
        {
          return false;
        }
 }
}