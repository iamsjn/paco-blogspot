<?php
/**
 * Description of Blog Model
 *
 * @author Khairul Islam
 */
class Blog_model extends CI_Model {
    
    public function __construct() 
    {
        parent::__construct();
        $this->load->database();
    }  
        
    function create_blog($data)
    {
        $this->db->set('date', 'NOW()', FALSE);
	$this->db->insert('user_blog', $data);
        return true;
    }
    
    function update_blog($data, $id)
    {
        $this->db->where('user_id', $id);
        $this->db->update('user_blog', $data); 
        return true;
    }    
    
   
 function get_blog_data($data)
 {
        $this->db->select('blog_title, blog_sub_title');
        $this->db->from('user_blog');
        $this->db->where('user_id', $data);
        $this->db->limit(1);
        $query = $this->db->get();

        if($query -> num_rows() >= 1)
        {
          return $query->result();
        }
        else
        {
          return false;
        }
 }  
    
}
