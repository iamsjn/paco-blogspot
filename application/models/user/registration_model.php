<?php
/**
 * Description of post_model
 *
 * @author Khairul Islam
 */
class Registration_model extends CI_Model {
    
    public function __construct() 
    {
        parent::__construct();
        $this->load->database();
    }  
        
    function save_user_data($data)
    {
        $this->db->set('date', 'NOW()', FALSE);
	$this->db->insert('user_info', $data);
        return $this->db->insert_id();
    }
    
    function profile_update($data, $id)
    {
        $this->db->where('id', $id);
        $array = array('user_first_name' => $data['user_first_name'], 'user_last_name' => $data['user_last_name'], 'user_email' => $data['user_email']);        
        $this->db->set($array);        
        $this->db->update('user_info'); 
        return true;
    }
    
    function password_update($data, $id)
    {
        $this->db->where('id', $id);
        $this->db->set('user_password', $data['user_password']);        
        $this->db->update('user_info'); 
        return true;
    }
    
    function update_profile_photo($data, $id)
    {
        $this->db->where('id', $id);
        $array = array('user_main_photo' => $data['user_main_photo'], 'user_photo' => $data['user_photo']);                
        $this->db->set($array);        
        $this->db->update('user_info'); 
        return true;
    }     
    
 function check_user_name($data)
 {
   $this->db->select('id, user_name');
   $this->db->from('user_info');
   $this->db->where('user_name', $data);
   $this->db->limit(1);
   $query = $this->db->get();
 
   if($query -> num_rows() >= 1)
   {
     return $query->result();
   }
   else
   {
     return false;
   }
 }
 
 function get_commenter_info($data)
 {
   $this->db->select('user_name, user_first_name, user_last_name');
   $this->db->from('user_info');
   $this->db->where('id', $data);
   $query = $this->db->get();
   
     return $query->result();
 } 
    
}
