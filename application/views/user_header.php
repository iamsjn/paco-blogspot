<!DOCTYPE html>
<html lang="en">
    <head>
	<meta charset="UTF-8" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"> 
        <title>Paco-Blogspot</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0"> 
        <meta name="description" content="" />
        <meta name="keywords" content="" />
        <meta name="author" content="" />
        <link rel="shortcut icon" href=""> 
	<link href='http://fonts.googleapis.com/css?family=Bree+Serif|Open+Sans+Condensed:700,300,300italic' rel='stylesheet' type='text/css'>
        <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/login_page/css/bootstrap.min.css'); ?>" />
        <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/login_page/css/demo.css'); ?>" />
        <?php
        if (!$this->session->userdata('user_name')):
        ?>        
        <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/login_page/css/style2.css'); ?>" />
        <?php
        endif;
        ?>        
	<script src="<?php echo base_url('assets/login_page/js/modernizr.custom.72111.js'); ?>"></script>
        <script src="<?php echo base_url('assets/login_page/js/jquery.min.js'); ?>"></script>     
        <script src="<?php echo base_url('assets/login_page/js/jquery.js'); ?>"></script>
        <script src="<?php echo base_url('assets/login_page/js/jquery.validate.js'); ?>"></script>

<style>
    .no-cssanimations .rw-wrapper .rw-sentence span:first-child{
    opacity: 1;
    }
    .display{
    display: none;
    }
    .input-box{
    margin-bottom: -1.5%;
    }
    .thumbnail {
    position:relative;
    overflow:hidden;
    }
 
    .caption {
    position:absolute;
    top:0;
    right:0;
    background:rgba(66, 139, 202, 0.75);
    width:100%;
    height:100%;
    padding:2%;
    display: none;
    text-align:center;
    color:#fff !important;
    z-index:2;
    }
    .hr-margin{
	margin-top: 1px;
	margin-bottom: 1px;
	}
    .logout>li>a{
        cursor: pointer;
        }        
    .logout>li>a:hover{
        color: #0A68FA;
        background-color: rgba(127, 255, 212, 0);                    
        }
        .uname:hover{
        border-bottom: 1px solid;
        text-decoration: none;             
        }
    .slide {
        width: auto;
        height: 100%;
        padding: 0;
        display: none; 
        position: absolute;
        bottom: 0px;
        background-color: rgba(0, 0, 0, 0.5);
        left:-2px;
    }        
</style>
<script>
$(document).ready(function(){
   
    $(".slide_image").hover(function(){
        $(this).find(".snipit").slideDown("slow");
    },function(){
        $(this).find(".snipit").slideUp("slow");    
    });
 
    
});
</script>
<script>
    $(document).ready(function() {
    $('.index').removeClass("display");
    $('.noscript').addClass("display");     
    $('.panel-2').addClass("display");
    $('.admin-panel').addClass("display");     
    $('#showpanel-1').click(function() {
	$( ".panel-2" ).addClass( "display" );
	$( ".panel-1" ).removeClass( "display" );				
    });
    $('#showpanel-2').click(function() {
        $( ".panel-1" ).addClass( "display" );
	$( ".panel-2" ).removeClass( "display" );
    });
    $("[rel='tooltip']").tooltip();    
    $('.thumbnail').hover(
        function(){
            $(this).find('.caption').slideDown(250); //.fadeIn(250)
        },
        function(){
            $(this).find('.caption').slideUp(250); //.fadeOut(205)
        }
    );     
        });    
</script>
<script>
    $(document).ready(function() {
    $('.profile').addClass("display");
    $('.password').addClass("display");
    $('.post-list').addClass("display");
    $('.new-post').addClass("display");
    $('.theme').addClass("display");      
    $('#showprofile').click(function() {
	$( ".dashboard" ).addClass( "display" );
        $('.password').addClass("display");        
	$( ".post-list" ).addClass( "display" );
	$( ".new-post" ).addClass( "display" );
	$( ".theme" ).addClass( "display" );           
	$( ".profile" ).removeClass( "display" );				
    });
    $('#showdashboard').click(function() {
        $( ".profile" ).addClass( "display" );
        $('.password').addClass("display");    
	$( ".post-list" ).addClass( "display" );
	$( ".new-post" ).addClass( "display" );
	$( ".theme" ).addClass( "display" );           
	$( ".dashboard" ).removeClass( "display" );
    });
    $('#showpostlist').click(function() {
        $( ".profile" ).addClass( "display" );
        $('.password').addClass("display");    
	$( ".dashboard" ).addClass( "display" );
	$( ".new-post" ).addClass( "display" );
	$( ".theme" ).addClass( "display" );           
	$( ".post-list" ).removeClass( "display" );        
    });
    $('#showpost').click(function() {
        $( ".profile" ).addClass( "display" );
        $('.password').addClass("display");    
	$( ".dashboard" ).addClass( "display" );
	$( ".post-list" ).addClass( "display" ); 
	$( ".theme" ).addClass( "display" );           
	$( ".new-post" ).removeClass( "display" );        
    });
    $('#showtheme').click(function() {
        $( ".profile" ).addClass( "display" );
        $('.password').addClass("display");    
	$( ".dashboard" ).addClass( "display" );
	$( ".post-list" ).addClass( "display" );        
	$( ".new-post" ).addClass( "display" );
	$( ".theme" ).removeClass( "display" );        
    });
    $('#showpassword').click(function() {
        $( ".profile" ).addClass( "display" );
        $('.theme').addClass("display");    
	$( ".dashboard" ).addClass( "display" );
	$( ".post-list" ).addClass( "display" );        
	$( ".new-post" ).addClass( "display" );
	$( ".password" ).removeClass( "display" );        
    });    
        });    
</script>
<script type="text/javascript">
        setTimeout(function() { window.location.reload(); }, 7200000);
</script>
<!--[if lt IE 9]> 
<style>
.rw-wrapper{ display: none; } 
.rw-sentence-IE{ display: block;  }
</style>
<![endif]-->
</head>
<body>
    
<div class="navbar navbar-default navbar-fixed-top">
  <div class="navbar-header">
    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-responsive-collapse">
      <span class="icon-bar"></span>
      <span class="icon-bar"></span>
      <span class="icon-bar"></span>
    </button>
    <a class="navbar-brand" href="<?php echo site_url();?>">PacoBlogSpot.com</a>
  </div>
    
<?php
    if ($this->session->userdata('user_name')):
?>
    
  <div class="navbar-collapse collapse navbar-responsive-collapse">    
                            <ul class="nav navbar-nav navbar-right">
                            <?php
                            if ($this->uri->segment(1)!=""):
                            ?>
                                <li>
                                    <a href="<?php echo site_url();?>"><i class="glyphicon glyphicon-share-alt"></i> &nbsp;Goto Dashboard</a>
                                </li>                                
                            <?php
                            elseif ($this->session->userdata('blog_title')):
                            ?>                                
                                <li>
                                    <a href="<?php echo site_url() . $this->session->userdata('user_name');?>"><i class="glyphicon glyphicon-share-alt"></i> &nbsp;<?=$this->session->userdata('blog_title')?></a>
                                </li>
                            <?php endif; ?>    
                                <li class="dropdown">
                                    <a href="" class="dropdown-toggle" data-toggle="dropdown" href="#" role="button" aria-expanded="false"> <?=$this->session->userdata('user_name')?> <i class="caret"></i></a>
                                    <ul class="dropdown-menu logout" role="menu">
                                        <li id="logout"><a><i class="glyphicon glyphicon-off"></i> &nbsp;Logout</a></li>
                                    </ul>
                                </li>
                            </ul>
  </div>
    
<?php
    else:
?> 
    
  <div class="navbar-collapse collapse navbar-responsive-collapse">
    <ul class="nav navbar-nav navbar-right">               
        <li><a id="showpanel-1" style="cursor: pointer;">Sign in</a></li>
        <li><a id="showpanel-2" style="cursor: pointer;">Sign up</a></li>      
    </ul>
<?php
    endif;
?>      
  </div>
</div>