<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>SUJON | ADMIN</title>
    <link rel="shortcut icon" href="<?php echo base_url('assets/images/sujon_icon.jpg')?>">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/admin/bootstrap/css/bootstrap.min.css')?>" />
    <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/admin/font-awesome/css/font-awesome.min.css')?>" />
    <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/admin/css/local.css')?>" />

    <script type="text/javascript" src="<?php echo base_url('assets/admin/js/jquery-1.10.2.min.js')?>"></script>
    <script type="text/javascript" src="<?php echo base_url('assets/admin/bootstrap/js/bootstrap.min.js')?>"></script>

    <!-- you need to include the shieldui css and js assets in order for the charts to work -->
    <link rel="stylesheet" type="text/css" href="http://www.shieldui.com/shared/components/latest/css/shieldui-all.min.css" />
    <link rel="stylesheet" type="text/css" href="http://www.shieldui.com/shared/components/latest/css/light-bootstrap/all.min.css" />
    <script type="text/javascript" src="http://www.shieldui.com/shared/components/latest/js/shieldui-all.min.js"></script>
    <script type="text/javascript" src="http://www.prepbootstrap.com/Content/js/gridData.js"></script>
</head>
<body>
    <div id="wrapper">         
        <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">            
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="<?=base_url('admin/home')?>">Admin Panel</a>
            </div>
            <div class="collapse navbar-collapse navbar-ex1-collapse">
                <ul class="nav navbar-nav side-nav">
                    <li><a href="<?=base_url('admin/post')?>"><i class="fa fa-bullseye"></i> Posts</a></li>
                    <li><a href="<?=base_url('admin/newpost')?>"><i class="fa fa-tasks"></i> New Post</a></li>                    
                    <li><a href="<?=base_url('admin/project')?>"><i class="fa fa-globe"></i> Projects</a></li>
                    <li><a href="<?=base_url('admin/newproject')?>"><i class="fa fa-list-ol"></i> New Project</a></li>
                    <li><a href="<?=base_url('admin/messages')?>"><i class="fa fa-font"></i> Messages</a></li>
                    <li><a href="<?=base_url('admin/mail')?>"><i class="fa fa-font"></i> New Mail</a></li>                    
                </ul>
                <ul class="nav navbar-nav navbar-right navbar-user">
                    <li class="dropdown user-dropdown">
                       <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-user"></i>&nbsp <?=$username?> <b class="caret"></b></a>
                       <ul class="dropdown-menu">
                           <li><a href="<?=base_url('admin/admin/logout')?>"><i class="fa fa-power-off"></i> Log Out </a></li>
                       </ul>
                   </li>
                </ul>
            </div>
        </nav>