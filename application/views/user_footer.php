<h2 class="rw-sentence-IE">Own Blog is like creating beautiful butterflies with a silent touch of spice</h2>


<script type="text/javascript">
$(document).ready(function(){
$('#user_name').keyup(username_check);   
$('#user_name').blur(username_check);
$('#logout').click(user_logout);
});
     
function username_check(){
     
var username = $('#user_name').val();
jQuery.ajax({
    type: "POST",
    url: "<?php echo base_url('registration/check_user_name'); ?>",
    data: 'user_name='+ username,
    cache: false,
      success: function(data) {
           var obj = $.parseJSON(data);
                if(obj!=null)
                            {   
                                $('#uname_err').text("");                            
                                $('#uname_err').html(obj['user_name']);
                            }
    
                else{
                             $('#uname_err').text("");                            
                             $('#uname_err').html(obj);
                    }
 
}
});
}

function user_logout(){
jQuery.ajax({
    type: "POST",
    url: "<?php echo base_url('index/logout'); ?>",
      success: function(data) {
           var obj = $.parseJSON(data);
                if(obj!=null)
                            {   
                                window.location.href = "http://paco-blogspot.com/";
                            }
 
}
});
}
</script>


<script>
 $(document).ready(function () {

     $("#signupForm").validate({
         ignore: ":hidden",
         rules: {
             user_name: {
                 required: true,
                 minlength: 3
             },
             user_password: {
		required: true,
		minlength: 8
             },
             re_password: {
		required: true,
		minlength: 8,
		equalTo: "#user_password"
             },
            user_email: {
		required: true,
                minlength: 8,
		email: true
            }           
         },
submitHandler: function (form) {
    $('#loadingmessage').show();             
    $.ajax({
    type: "POST",
    url: "<?php echo base_url('registration/sign_up'); ?>",
    data: $(form).serialize(),
    cache: false,
       success: function(data) {
            $('#loadingmessage').hide();            
           var obj = $.parseJSON(data);
                if(obj['username']!=null || obj['user_password']!=null || obj['re_password']!=null || obj['user_email']!=null || obj['user_gender']!=null || obj['agree']!=null)
                            {   
                                $('#uname_err').text("");                            
                                $('#uname_err').html(obj['user_name']);
                                $('#pass_err').text("");
                                $('#pass_err').html(obj['user_password']);
                                $('#repass_err').text("");
                                $('#repass_err').html(obj['re_password']);
                                $('#email_err').text("");
                                $('#email_err').html(obj['user_email']);
                                $('#gender_err').text("");
                                $('#gender_err').html(obj['user_gender']);                                
                                $('#agree_err').text("");
                                $('#agree_err').html(obj['agree']);                                
                            }
                if(obj['redload']!=null)
                            {                               
                                window.location.reload(); 
                            }
    
            }
            });
        return false; // required to block normal submit since you used ajax
         }

     });

 });
</script>


<script>
 $(document).ready(function () {

     $("#signinForm").validate({
         ignore: ":hidden",
         rules: {
             user_name_1: {
                 required: true,
                 minlength: 3
             },
             user_password_1: {
                 required: true,
                 minlength: 8
             }             
         },
submitHandler: function (form) {
    $('#loadingmessage_3').show();             
    $.ajax({
    type: "POST",
    url: "<?php echo base_url('index/login'); ?>",
    data: $(form).serialize(),
    cache: false,
       success: function(data) {
            $('#loadingmessage_3').hide();            
           var obj = $.parseJSON(data);
                if(obj['login_err']!=null)
                            {   
                                $('#login_err').show();                           
                            }
                            
                if(obj['login_success']!=null)
                            {
                                window.location.reload();                                  
                            }
    
            }
            });
        return false; // required to block normal submit since you used ajax
         }

     });

 });
</script>

<script>
 $(document).ready(function () {

     $("#blogForm").validate({
         ignore: ":hidden",
         rules: {
             blog_title: {
                 required: true,
                 minlength: 10
             },
             sub_title: {
                 required: true,
                 minlength: 5
             }             
         },
submitHandler: function (form) {
    $('#loadingmessage_1').show();             
    $.ajax({
    type: "POST",
    url: "<?php echo base_url('blog/index'); ?>",
    data: $(form).serialize(),
    cache: false,
       success: function(data) {
            $('#loadingmessage_1').hide();            
           var obj = $.parseJSON(data);
                if(obj['blog_title']!=null || obj['sub_title']!=null)
                            {   
                                $('#blog_title_err').text("");                            
                                $('#blog_title_err').html(obj['blog_title']);
                                $('#sub_title_err').text("");
                                $('#sub_title_err').html(obj['sub_title']);                              
                            }
                if(obj['msg']!=null)
                            {
                                window.location.reload();                                  
                            }
    
            }
            });
        return false; // required to block normal submit since you used ajax
         }

     });

 });
</script>

<script>
 $(document).ready(function () {

     $("#blogForm2").validate({
         ignore: ":hidden",
         rules: {
             blog_title: {
                 required: true,
                 minlength: 10
             },
             sub_title: {
                 required: true,
                 minlength: 5
             }             
         },
submitHandler: function (form) {
    $('#loadingmessage_2').show();             
    $.ajax({
    type: "POST",
    url: "<?php echo base_url('blog/update'); ?>",
    data: $(form).serialize(),
    cache: false,
       success: function(data) {
            $('#loadingmessage_2').hide();            
           var obj = $.parseJSON(data);
                if(obj['blog_title']!=null || obj['sub_title']!=null)
                            {   
                                $('#blog_title_err').text("");                            
                                $('#blog_title_err').html(obj['blog_title']);
                                $('#sub_title_err').text("");
                                $('#sub_title_err').html(obj['sub_title']);                              
                            }
                if(obj['msg']!=null)
                            {
                                window.location.reload();                                  
                            }
    
            }
            });
        return false; // required to block normal submit since you used ajax
         }

     });

 });
</script>

<script>
 $(document).ready(function () {

     $("#profileForm").validate({
         ignore: ":hidden",
         rules: {
             fname: {
                 required: true,
                 minlength: 3
             },
             lname: {
                 required: true,
                 minlength: 3
             },             
            email_1: {
		required: true,
                minlength: 8,
		email: true
            }           
         },
submitHandler: function (form) {
    $('#loadingmessage_4').show();             
    $.ajax({
    type: "POST",
    url: "<?php echo base_url('registration/profile_update'); ?>",
    data: $(form).serialize(),
    cache: false,
       success: function(data) {
            $('#loadingmessage_4').hide();            
           var obj = $.parseJSON(data);
                if(obj['fname_err']!=null || obj['lname_err']!=null || obj['email_err']!=null)
                            {   
                                $('#fname_err').text("");                            
                                $('#fname_err').html(obj['fname_err']);
                                $('#lname_err').text("");
                                $('#lname_err').html(obj['lname_err']);
                                $('#email_err').text("");
                                $('#email_err').html(obj['email_err']);                                 
                            }
                if(obj['msg']!=null)
                            {
                                window.location.reload();                                  
                            }
    
            }
            });
        return false; // required to block normal submit since you used ajax
         }

     });

 });
</script>

<script>
 $(document).ready(function () {

     $("#passwordForm").validate({
         ignore: ":hidden",
         rules: {
             password_1: {
                 required: true,
                 minlength: 8
             },
             re_password_1: {
                 required: true,
                 minlength: 8,
                 equalTo: "#password_1"
             }          
         },
submitHandler: function (form) {
    $('#loadingmessage_5').show();             
    $.ajax({
    type: "POST",
    url: "<?php echo base_url('registration/password_change'); ?>",
    data: $(form).serialize(),
    cache: false,
       success: function(data) {
            $('#loadingmessage_5').hide();            
           var obj = $.parseJSON(data);
                if(obj['pass_1_err']!=null || obj['repass_1_err']!=null)
                            {   
                                $('#pass_1_err').text("");                            
                                $('#pass_1_err').html(obj['pass_1_err']);
                                $('#repass_1_err').text("");
                                $('#repass_1_err').html(obj['repass_1_err']);                                
                            }
                if(obj['msg']!=null)
                            {
                                window.location.reload();                                  
                            }
    
            }
            });
        return false; // required to block normal submit since you used ajax
         }

     });

 });
</script>

<script>
 $(document).ready(function () {

     $("#newpostForm").validate({
         ignore: ":hidden",
         rules: {
             title: {
                 required: true,
                 minlength: 3
             },
             content: {
                 required: true,
                 minlength: 10
             }          
         },
submitHandler: function (form) {
    $('#loadingmessage_6').show();             
    $.ajax({
    type: "POST",
    url: "<?php echo base_url('blog_post/index'); ?>",
    data: $(form).serialize(),
    cache: false,
       success: function(data) {
            $('#loadingmessage_6').hide();            
           var obj = $.parseJSON(data);
                if(obj['title']!=null || obj['content']!=null)
                            {   
                                $('#title_err').text("");                            
                                $('#title_err').html(obj['title']);
                                $('#content_err').text("");
                                $('#content_err').html(obj['content']);                                
                            }
                if(obj['msg']!=null)
                            {
                                window.location.reload();                                  
                            }
    
            }
            });
        return false; // required to block normal submit since you used ajax
         }

     });

 });
</script>

<script>
$("form#profilePictureForm").submit(function(){

    var formData = new FormData($(this)[0]);

    $.ajax({
        url: "<?php echo base_url('registration/update_profile_picture'); ?>",
        type: 'POST',
        data: formData,
        async: false,
        success: function (data) {
           var obj = $.parseJSON(data);
                if(obj['userfile_err']!=null)
                            {
                                $('#userfile_err').text("");                            
                                $('#userfile_err').html(obj['userfile_err']);                             
                            }           
           
                if(obj['msg']!=null)
                            {
                                window.location.reload();                                  
                            }
        },
        cache: false,
        contentType: false,
        processData: false
    });

    return false;
});
</script>

<script>
 $(document).ready(function () {

     $("#commentForm").validate({
         ignore: ":hidden",
         rules: {
             comment: {
                 required: true,
                 minlength: 2
             }          
         },
submitHandler: function (form) {            
    $.ajax({
    type: "POST",
    url: "<?php echo base_url('blog_post/comment'); ?>",
    data: $(form).serialize(),
    cache: false,
       success: function(data) {            
           var obj = $.parseJSON(data);
                if(obj['comment_err']!=null)
                            {   
                                $('#comment_err').text("");                            
                                $('#comment_err').html(obj['comment_err']);                                
                            }
                if(obj['comment_success']!=null)
                            {   $('#comment_show').append("<h2><a href=''>" + obj['commenter_fname'] + "&nbsp" + obj['commenter_lname'] +"</a><small>&nbsp(Just now)</small></h2>");                      
                                $('#comment_show').append("<div>" + obj['comment_success'] + "</div>" + "<br/>" );
                                $('#comment').val("");
                            }
    
            }
            });
        return false; // required to block normal submit since you used ajax
         }

     });

 });
</script>
<script src="<?php echo base_url('assets/login_page/js/bootstrap.min.js'); ?>"></script>
    
</body>
</html>