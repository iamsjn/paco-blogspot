<!DOCTYPE html>
<html lang="en">
    <head>
	<meta charset="UTF-8" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"> 
        <title>Paco-Blogspot</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0"> 
        <meta name="description" content="" />
        <meta name="keywords" content="" />
        <meta name="author" content="" />
        <link rel="shortcut icon" href=""> 
	<link href='http://fonts.googleapis.com/css?family=Bree+Serif|Open+Sans+Condensed:700,300,300italic' rel='stylesheet' type='text/css'>
        <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/login_page/css/bootstrap.min.css'); ?>" />
        <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/login_page/css/demo.css'); ?>" />
        <?php
        if (!$this->session->userdata('user_name')):
        ?>        
        <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/login_page/css/style2.css'); ?>" />
        <?php
        endif;
        ?>        
	<script src="<?php echo base_url('assets/login_page/js/modernizr.custom.72111.js'); ?>"></script>
        <script src="<?php echo base_url('assets/login_page/js/jquery.min.js'); ?>"></script>     
        <script src="<?php echo base_url('assets/login_page/js/jquery.js'); ?>"></script>
        <script src="<?php echo base_url('assets/login_page/js/jquery.validate.js'); ?>"></script>       
<style>
    .no-cssanimations .rw-wrapper .rw-sentence span:first-child{
    opacity: 1;
    }
    .display{
    display: none;
    }
    .input-box{
    margin-bottom: -1.5%;
    }
    .thumbnail {
    position:relative;
    overflow:hidden;
    }
 
    .caption {
    position:absolute;
    top:0;
    right:0;
    background:rgba(66, 139, 202, 0.75);
    width:100%;
    height:100%;
    padding:2%;
    display: none;
    text-align:center;
    color:#fff !important;
    z-index:2;
    }
    .hr-margin{
	margin-top: 1px;
	margin-bottom: 1px;
	}
    .logout>li>a{
        cursor: pointer;
        }        
    .logout>li>a:hover{
        color: #0A68FA;
        background-color: rgba(127, 255, 212, 0);                    
        }
        .uname:hover{
        border-bottom: 1px solid;
        text-decoration: none;             
        }           
</style>
<script>
    $(document).ready(function() {
    $('.index').removeClass("display");
    $('.noscript').addClass("display");     
    $('.panel-2').addClass("display");
    $('.admin-panel').addClass("display");     
    $('#showpanel-1').click(function() {
	$( ".panel-2" ).addClass( "display" );
	$( ".panel-1" ).removeClass( "display" );				
    });
    $('#showpanel-2').click(function() {
        $( ".panel-1" ).addClass( "display" );
	$( ".panel-2" ).removeClass( "display" );
    });
    $("[rel='tooltip']").tooltip();    
    $('.thumbnail').hover(
        function(){
            $(this).find('.caption').slideDown(250); //.fadeIn(250)
        },
        function(){
            $(this).find('.caption').slideUp(250); //.fadeOut(205)
        }
    );     
        });    
</script>
<script>
    $(document).ready(function() {
    $('.profile').addClass("display");
    $('.password').addClass("display");
    $('.post-list').addClass("display");
    $('.new-post').addClass("display");
    $('.theme').addClass("display");      
    $('#showprofile').click(function() {
	$( ".dashboard" ).addClass( "display" );
        $('.password').addClass("display");        
	$( ".post-list" ).addClass( "display" );
	$( ".new-post" ).addClass( "display" );
	$( ".theme" ).addClass( "display" );           
	$( ".profile" ).removeClass( "display" );				
    });
    $('#showdashboard').click(function() {
        $( ".profile" ).addClass( "display" );
        $('.password').addClass("display");    
	$( ".post-list" ).addClass( "display" );
	$( ".new-post" ).addClass( "display" );
	$( ".theme" ).addClass( "display" );           
	$( ".dashboard" ).removeClass( "display" );
    });
    $('#showpostlist').click(function() {
        $( ".profile" ).addClass( "display" );
        $('.password').addClass("display");    
	$( ".dashboard" ).addClass( "display" );
	$( ".new-post" ).addClass( "display" );
	$( ".theme" ).addClass( "display" );           
	$( ".post-list" ).removeClass( "display" );        
    });
    $('#showpost').click(function() {
        $( ".profile" ).addClass( "display" );
        $('.password').addClass("display");    
	$( ".dashboard" ).addClass( "display" );
	$( ".post-list" ).addClass( "display" ); 
	$( ".theme" ).addClass( "display" );           
	$( ".new-post" ).removeClass( "display" );        
    });
    $('#showtheme').click(function() {
        $( ".profile" ).addClass( "display" );
        $('.password').addClass("display");    
	$( ".dashboard" ).addClass( "display" );
	$( ".post-list" ).addClass( "display" );        
	$( ".new-post" ).addClass( "display" );
	$( ".theme" ).removeClass( "display" );        
    });
    $('#showpassword').click(function() {
        $( ".profile" ).addClass( "display" );
        $('.theme').addClass("display");    
	$( ".dashboard" ).addClass( "display" );
	$( ".post-list" ).addClass( "display" );        
	$( ".new-post" ).addClass( "display" );
	$( ".password" ).removeClass( "display" );        
    });    
        });    
</script>
<!--[if lt IE 9]> 
<style>
.rw-wrapper{ display: none; } 
.rw-sentence-IE{ display: block;  }
</style>
<![endif]-->
</head>
<body>
    
<div class="navbar navbar-default">
  <div class="navbar-header">
    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-responsive-collapse">
      <span class="icon-bar"></span>
      <span class="icon-bar"></span>
      <span class="icon-bar"></span>
    </button>
    <a class="navbar-brand" href="#">PacoBlogSpot.com</a>
  </div>
    
<?php
    if ($this->session->userdata('user_name')):
?>
    
  <div class="navbar-collapse collapse navbar-responsive-collapse">    
                            <ul class="nav navbar-nav navbar-right">
                            <?php
                            if ($this->session->userdata('blog_title')):
                            ?>                                
                                <li>
                                    <a href=""><i class="glyphicon glyphicon-share-alt"></i> &nbsp;<?=$this->session->userdata('blog_title')?></a>
                                </li>
                            <?php endif; ?>    
                                <li class="dropdown">
                                    <a href="" class="dropdown-toggle" data-toggle="dropdown" href="#" role="button" aria-expanded="false"> <?=$this->session->userdata('user_name')?> <i class="caret"></i></a>
                                    <ul class="dropdown-menu logout" role="menu">
                                        <li id="logout"><a><i class="glyphicon glyphicon-off"></i> &nbsp;Logout</a></li>
                                    </ul>
                                </li>
                            </ul>
  </div>
    
<?php
    else:
?> 
    
  <div class="navbar-collapse collapse navbar-responsive-collapse">
    <ul class="nav navbar-nav navbar-right">               
        <li><a id="showpanel-1" style="cursor: pointer;">Sign in</a></li>
        <li><a id="showpanel-2" style="cursor: pointer;">Sign up</a></li>      
    </ul>
<?php
    endif;
?>      
  </div>
</div>
<div class="col-md-6 col-md-offset-3 noscript">
    <div class="panel panel-default">
        <div class="panel-body" style="font-family: -webkit-pictograph;color: black;">
            <h2 class="text-center text-danger">Paco-Blogspot is not working!</h2>
            <h1 class="text-center text-danger">Please Enable Javascript On Your Browser!!!</h1>
        </div>
    </div>
</div> 
<?php
    if ($this->session->userdata('user_name')):
?>
<script>
    $(document).ready(function() {
    $('.index').addClass("display");
    $('.admin-panel').removeClass("display");    
    });      
</script>
<?php
    endif;
?>     
<div class="col-md-12 index display">
        <div class="row">
        <div class="col-md-9">
<section class="rw-wrapper">
<h2 class="rw-sentence">
    <span>Own Blog is like</span>
	<br />
            <span>creating</span>
		<div class="rw-words rw-words-1">
                    <span>breathtaking moments</span>
                    <span>lovely sounds</span>
                    <span>incredible magic</span>
                    <span>unseen experiences</span>
                    <span>happy feelings</span>
                    <span>beautiful butterflies</span>
		</div>
		<br />
		<span>with a silent touch of</span>
                    <div class="rw-words rw-words-2">
			<span>sugar</span>
			<span>spice</span>
			<span>colors</span>
			<span>happiness</span>
			<span>wonder</span>
			<span>happiness</span>
                </div>
	</h2>
</section>
</div>
<div class="col-md-3">    
    <div class="panel panel-default panel-1">
        <div class="panel-heading">
            <h1 class="panel-title text-center">Sign in</h1>
        </div>	
    <div class="panel-body">
        <div id='loadingmessage_3' class="display text-center">
        <img src='<?php echo base_url('assets/login_page/images/loading.gif');?>'/>
        </div>                    
        <h3 class='text-center text-danger display' id="login_err">Invalid Username and Password combination.</h3>       
                <?php echo form_open_multipart('index/login', array("role" => "form", "id" => "signinForm", "class" => "cmxform")); ?>      
                <div class="form-group">           
                <div class="form-group has-success">
                <?php
                echo form_label("Username", "user_name_1",array("class" => "control-label","style" => "color: #FFFFFF;font-weight:bold;"));
                echo form_input(array("name" => "user_name_1", "id" => "user_name_1", "class" => "form-control input-box","value" => set_value('user_name_1'), "required" => "required"));
                ?>  
                </div>
                <div class="form-group has-success">
                <?php
                echo form_label("Password", "user_password_1",array("class" => "control-label","style" => "color: #FFFFFF;font-weight:bold;"));
                echo form_password(array("name" => "user_password_1","id" => "user_password_1", "class" => "form-control input-box","value" => set_value('user_password_1'), "required" => "required"));
                ?>  
                </div>
                <div class="form-group has-success">
                <?php
                echo form_checkbox(array("name" => "remember_me", "value" => "1", "id" => "remember_me")) . "&nbsp";
                echo form_label("Remember me", "remember_me", array("class" => "control-label","style" => "color: #000000;font-weight:bold;"));                
                ?> 
                </div>       
                <span class="input-group-btn">
                <?php echo form_submit(array("name" => "login","id" => "login", "class" => "btn btn-primary", "value" => "Sign in")) ?>
                </span>
          </div>
            <?php echo form_close(); ?>         
      </div>       
</div>
<!--end of login panel -->    


<div class="panel panel-default panel-2">
        <div class="panel-heading">
        <h1 class="panel-title text-center">Sign up</h1>
        </div>
        <div class="panel-body">
        <div id='loadingmessage' class="display text-center">
        <img src='<?php echo base_url('assets/login_page/images/loading.gif');?>'/>
        </div>        
        <?php echo form_open_multipart('registration/index', array("role" => "form", "id" => "signupForm", "class" => "cmxform")); ?>    
        <div class="form-group">
        <?php
        if ($this->session->flashdata('submission_error')):
         echo "<div class='alert alert-success'>";
        echo "<button type='button' class='close' data-dismiss='alert'>&times;</button>";
        echo $this->session->flashdata('submission_error');
        echo "</div>";
        elseif ($this->session->userdata('sign_up')):
        echo "<span class='text-left text-danger'>".validation_errors()."</span>";
        $this->session->unset_userdata('sign_up');
        endif;
        ?>          
                <div class="form-group has-success">
                <?php
                echo form_label("Username", "user_name",array("class" => "control-label","style" => "color: #FFFFFF;font-weight:bold;"));
                echo form_input(array("name" => "user_name", "id" => "user_name", "class" => "input-box form-control","value" => set_value('user_name'), "required" => "required"));
                ?>  
                <span class='text-left text-danger' id='uname_err'></span>
                </div>
                <div class="form-group has-success">
                <?php
                echo form_label("Password", "user_password",array("class" => "control-label","style" => "color: #FFFFFF;font-weight:bold;"));
                echo form_password(array("name" => "user_password","id" => "user_password", "class" => "form-control input-box","value" => set_value('user_password'), "required" => "required"));
                ?>  
                <span class='text-left text-danger' id='pass_err'></span>
                </div>
                <div class="form-group has-success">
                <?php
                echo form_label("Re-Password", "re_password",array("class" => "control-label","style" => "color: #FFFFFF;font-weight:bold;"));
                echo form_password(array("name" => "re_password","id" => "re_password", "class" => "form-control input-box","value" => set_value('re_password'), "required" => "required"));
                ?>  
                <span class='text-left text-danger' id='repass_err'></span>
                </div>
                <div class="form-group has-success">
                <?php
                echo form_label("Email", "user_email",array("class" => "control-label","style" => "color: #FFFFFF;font-weight:bold;"));
                echo form_input(array("name" => "user_email","id" => "user_email", "class" => "form-control input-box","value" => set_value('user_email'), "required" => "required"));
                ?>  
                <span class='text-left text-danger' id='email_err'></span>
                </div>
                <div class="form-group has-success">
                <?php
                $data = array("name" => "user_gender","id" => "user_gender_female","value" => '0');
                echo form_radio($data) . "&nbsp";
                echo form_label("Female", "user_gender_female", array("class" => "control-label","style" => "color: #000000;font-weight:bold;")) . "&nbsp &nbsp &nbsp";                    

                $data = array("name" => "user_gender","id" => "user_gender_male","value" => '1');
                echo form_radio($data) . "&nbsp";
                echo form_label("Male", "user_gender_male", array("class" => "control-label","style" => "color: #000000;font-weight:bold;"));                    
                ?>
                <span class='text-left text-danger' id='gender_err' style="margin-top:-6%;"></span>
                </div>            
                <div class="form-group has-success">
                <?php
                $data = array("name" => "agree","id" => "agree","value" => '1');
                echo form_checkbox($data) . "&nbsp";
                echo form_label("Agree with terms and conditions", "agree", array("class" => "control-label","style" => "color: #000000;font-weight:bold;"));                    
                ?>
                <span class='text-left text-danger' id='agree_err' style="margin-top:-6%;"></span>
                </div>    
    
                <span class="input-group-btn">
                <?php echo form_submit(array("name" => "submit","id" => "submit", "class" => "btn btn-primary", "value" => "Sign up")) ?>
                </span>
                </div>
                <?php echo form_close(); ?>          
           </div>
        </div>    
    </div>
  </div>
</div>
<!--end of sign up panel -->

<!-- Modal -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog" style="font-family: -webkit-pictograph;color: black;">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
        <h4 class="modal-title text-center" id="myModalLabel">Update Profile Picture</h4>
      </div>
      <div class="modal-body">
                <?php echo form_open_multipart('registration/update_profile_picture', array("role" => "form", "id" => "profilePictureForm", "class" => "cmxform")); ?>      
                <div class="form-group">
                   
                <div class="form-group">
                <?php
                echo form_label("Upload", "photo",array("class" => "control-label","style" => "color: black;"));
                echo form_input(array("name" => "userfile","type"=> "file", "class" => "form-control"));
                ?>
                <span class='text-left text-danger' id='userfile_err'></span>    
                </div>                
                <span class="input-group-btn">
               <?php echo form_submit(array("name" => "update","id" => "update", "class" => "btn btn-info", "value" => "Update")) ?>
                </span>
                 </div>
                <?php echo form_close(); ?> 
      </div>
    </div>
  </div>
</div>

<div class="col-md-12 admin-panel display">
<div class="row">
  <div class="col-md-2 col-sm-4 col-xs-6">
    <a href="" class="thumbnail slide_image" data-toggle="modal" data-target="#myModal">
        <?php
        if ($this->session->userdata('user_photo')):
        ?>         
        <img src="<?php echo base_url($this->session->userdata('user_photo')); ?>" alt="User Photo">
        <div class="snipit slide">
            <button type="button" class="btn btn-default" style="white-space: normal;width: 100px;">Change Picture</button>
        </div>         
        <?php
        else:?>
        <img src="<?php echo base_url('assets/login_page/images/profile_default.png');?>"alt='User Photo'>
        <div class="snipit slide">
            <button type="button" class="btn btn-default" style="white-space: normal;width: 100px;">Change Picture</button>
        </div>        
        <?php endif;?>
    </a>
</div>
    <div class="col-md-7">
        <?php
        if ($this->session->userdata('user_fname') && $this->session->userdata('user_lname')):
        ?>        
        <h1><a class="uname" href="<?php echo site_url();?>"><?=$this->session->userdata('user_fname')?> <?=$this->session->userdata('user_lname')?></a></h1>
        <?php else:
        echo "<div class='alert alert-warning text-center col-md-offset-2'>";
        echo "<button type='button' class='close' data-dismiss='alert'>&times;</button>";
        echo "You have not yet complete your profile. Please complete your profile in profile section";
        echo "</div>";            
        endif;?>
    </div>
</div>
<hr style="margin-top:1px;"/>
    <div class="panel panel-default">
        <div class="panel-body" style="font-family: -webkit-pictograph;color: black;">
            <div class="col-md-2">
                <ul class="nav nav-pills nav-stacked">
                <li id="showdashboard">
                    <a style="cursor: pointer;"><i class="glyphicon glyphicon-home"></i> Dashboard</a>
                </li>
                <hr class="hr-margin"/>
                <li id="showprofile">
                    <a style="cursor: pointer;"><i class="glyphicon glyphicon-user"></i> Profile</a>
                </li>
                <hr class="hr-margin"/>
                <li id="showpassword">
                    <a style="cursor: pointer;"><i class="glyphicon glyphicon-cog"></i> Change Password</a>
                </li>                
                <hr class="hr-margin"/>                
                <li id="showpostlist">
                    <a style="cursor: pointer;"><i class="glyphicon glyphicon-list-alt"></i> Post List</a>
                </li>
                <hr class="hr-margin"/>                
                <li id="showpost">
                    <a style="cursor: pointer;"><i class="glyphicon glyphicon-pencil"></i> New Post</a>
                </li>
                <hr class="hr-margin"/>                
                <li id="showtheme">
                    <a style="cursor: pointer;"><i class="glyphicon glyphicon-picture"></i> Select Theme</a>
                </li>                
                </ul>
         </div>
            
                <?php
                if ($this->session->userdata('blog_title') && $this->session->userdata('sub_title')):
                ?>
            
            <div class="col-md-10 dashboard">
                <div class="panel panel-default" style="border: none;">
                <div class="panel-body">                      
                <div class="col-md-5">
                <?php
                if ($this->session->flashdata('success_msg')):
                 echo "<div class='alert alert-success text-center' style='padding: 5px;'>";
                echo "<button type='button' class='close' data-dismiss='alert'>&times;</button>";
                echo $this->session->flashdata('success_msg');
                echo "</div>";
                endif;
                ?>                    
                <div id='loadingmessage_2' class="display text-center">
                <img src='<?php echo base_url('assets/login_page/images/loading.gif');?>'/>
                </div>                      
                <?php echo form_open_multipart('blog/update', array("role" => "form", "id" => "blogForm2", "class" => "cmxform")); ?>      
                <div class="form-group">
                   
                <div class="form-group">
                <?php
                echo form_label("Blog-Title", "blog_title",array("class" => "control-label","style" => "color: black;"));
                echo form_input(array("name" => "blog_title", "id" => "blog_title", "class" => "form-control input-box","value" => $this->session->userdata('blog_title'), "required" => "required"));
                ?>
                <span class='text-left text-danger' id='blog_title_err'></span>                    
                </div>
                <div class="form-group">
                <?php
                echo form_label("Sub-Title", "sub_title",array("class" => "control-label","style" => "color: black;"));
                echo form_input(array("name" => "sub_title","id" => "sub_title", "class" => "form-control input-box","value" => $this->session->userdata('sub_title'), "required" => "required"));
                ?>
                <span class='text-left text-danger' id='sub_title_err'></span>                     
                </div>     
                <div class="form-group">
               <?php echo form_submit(array("name" => "update","id" => "update", "class" => "btn btn-info", "value" => "Update")); ?>                    
                </div>
                 </div>
                <?php echo form_close(); ?>  
                        </div>
                    </div>
                </div>
            </div>            
                <?php else:?>
            <div class="col-md-10 dashboard">
                <div class="panel panel-default" style="border: none;">
                <div class="panel-body">                      
                <div class="col-md-5">
                <?php
                if ($this->session->flashdata('success_msg')):
                 echo "<div class='alert alert-success text-center' style='padding: 5px;'>";
                echo "<button type='button' class='close' data-dismiss='alert'>&times;</button>";
                echo $this->session->flashdata('success_msg');
                echo "</div>";
                endif;
                ?>                    
                <div id='loadingmessage_1' class="display text-center">
                <img src='<?php echo base_url('assets/login_page/images/loading.gif');?>'/>
                </div>                      
                <?php echo form_open_multipart('blog/index', array("role" => "form", "id" => "blogForm", "class" => "cmxform")); ?>      
                <div class="form-group">
                    
                <div class="form-group">
                <?php
                echo form_label("Blog-Title", "blog_title",array("class" => "control-label","style" => "color: black;"));
                echo form_input(array("name" => "blog_title", "id" => "blog_title", "class" => "form-control input-box","value" => set_value('blog_title'), "required" => "required"));
                ?>
                <span class='text-left text-danger' id='blog_title_err'></span>                    
                </div>
                <div class="form-group">
                <?php
                echo form_label("Sub-Title", "sub_title",array("class" => "control-label","style" => "color: black;"));
                echo form_input(array("name" => "sub_title","id" => "sub_title", "class" => "form-control input-box","value" => set_value('sub_title'), "required" => "required"));
                ?>
                <span class='text-left text-danger' id='sub_title_err'></span>                     
                </div>     
                <span class="input-group-btn">
               <?php echo form_submit(array("name" => "create","id" => "create", "class" => "btn btn-info", "value" => "Create")) ?>
                </span>
                 </div>
                <?php echo form_close(); ?>  
                        </div>
                    </div>
                </div>
            </div>
                <?php endif; ?>
            
            <div class="col-md-10 profile">
                <div class="panel panel-default" style="border: none;">
                    <div class="panel-body">
                       
                <div class="col-md-5">
                <div id='loadingmessage_4' class="display text-center">
                <img src='<?php echo base_url('assets/login_page/images/loading.gif');?>'/>
                </div>                    
                <?php echo form_open_multipart('registration/profile_update', array("role" => "form", "id" => "profileForm", "class" => "cmxform")); ?>      
                <div class="form-group">
                    
                <div class="form-group">
                <?php
                if ($this->session->userdata('user_fname')):
                                 
                echo form_label("First Name", "fname",array("class" => "control-label","style" => "color: black;"));
                echo form_input(array("name" => "fname", "id" => "fname", "class" => "form-control input-box","value" => $this->session->userdata('user_fname'), "required" => "required"));

                else:
                echo form_label("First Name", "fname",array("class" => "control-label","style" => "color: black;"));
                echo form_input(array("name" => "fname", "id" => "fname", "class" => "form-control input-box","value" => set_value('fname'), "required" => "required"));
                    
                endif;
                ?>
                <span class='text-left text-danger' id='fname_err'></span>                    
                </div>
                <div class="form-group">
                <?php
                
                if($this->session->userdata('user_lname')):
                echo form_label("Last Name", "lname",array("class" => "control-label","style" => "color: black;"));
                echo form_input(array("name" => "lname","id" => "lname", "class" => "form-control input-box","value" => $this->session->userdata('user_lname'), "required" => "required"));
                
                else:
                echo form_label("Last Name", "lname",array("class" => "control-label","style" => "color: black;"));
                echo form_input(array("name" => "lname","id" => "lname", "class" => "form-control input-box","value" => set_value('lname'), "required" => "required"));
                    
                endif;
                ?>
                <span class='text-left text-danger' id='lname_err'></span>                    
                </div>
                <div class="form-group">
                <?php
                
                if($this->session->userdata('user_email')):
                echo form_label("Email", "email_1",array("class" => "control-label","style" => "color: black;"));
                echo form_input(array("name" => "email_1","id" => "email_1", "class" => "form-control input-box","value" => $this->session->userdata('user_email'), "required" => "required"));
                
                else:
                echo form_label("Email", "email_1",array("class" => "control-label","style" => "color: black;"));
                echo form_input(array("name" => "email_1","id" => "email_1", "class" => "form-control input-box","value" => set_value('email_1'), "required" => "required"));
                    
                endif;
                ?>
                <span class='text-left text-danger' id='email_err'></span>                    
                </div>                    
               <?php echo form_submit(array("name" => "update","id" => "update", "class" => "btn btn-info", "value" => "Update")) ?>                    
                 </div>
                <?php echo form_close(); ?>
               </div>
                    </div>
                </div>
            </div>
            
            
            <div class="col-md-10 password">
                <div class="panel panel-default" style="border: none;">
                    <div class="panel-body">
         
                <div class="col-md-5">
                <div id='loadingmessage_5' class="display text-center">
                <img src='<?php echo base_url('assets/login_page/images/loading.gif');?>'/>
                </div>                   
                <?php echo form_open_multipart('registration/profile_update', array("role" => "form", "id" => "passwordForm", "class" => "cmxform")); ?>                    
                <div class="form-group">
                <div class="form-group">
                <?php
                echo form_label("New Password", "password_1",array("class" => "control-label","style" => "color: black;"));
                echo form_password(array("name" => "password_1","id" => "password_1", "class" => "form-control input-box","value" => set_value('password_1'), "required" => "required"));
                ?>
                <span class='text-left text-danger' id='pass_1_err'></span>                    
                </div>
                <div class="form-group">
                <?php
                echo form_label("Re-Password", "re_password_1",array("class" => "control-label","style" => "color: black;"));
                echo form_password(array("name" => "re_password_1","id" => "re_password_1", "class" => "form-control input-box","value" => set_value('re_password_1'), "required" => "required"));
                ?>
                <span class='text-left text-danger' id='repass_1_err'></span>                    
                </div>                    
               <?php echo form_submit(array("name" => "update","id" => "update", "class" => "btn btn-info", "value" => "Update")) ?>                    
                 </div>
                <?php echo form_close(); ?>  
                        </div>
                    </div>
                </div>
            </div>            
            
            
            <div class="col-md-10 post-list">
                <div class="panel panel-default" style="border: none;">
                    <div class="panel-body">
                        <table class="table bootstrap-admin-table-with-actions" id="result_table">

                                    </table>
                    </div>
                </div>
            </div>
            
            
            <div class="col-md-10 new-post">
                <div class="panel panel-default" style="border: none;">
                    <div class="panel-body">
                        <div class="col-md-6">
                <div id='loadingmessage_6' class="display text-center">
                <img src='<?php echo base_url('assets/login_page/images/loading.gif');?>'/>
                </div>                            
                <?php echo form_open_multipart('blog_post/index', array("role" => "form", "id" => "newpostForm", "class" => "cmxform")); ?>      
                <div class="form-group">
                <div class="form-group">
                <?php
                echo form_label("Title (maximum 100 words)", "title",array("class" => "control-label","style" => "color: black;"));
                echo form_input(array("name" => "title","id" => "title", "class" => "form-control input-box","value" => set_value('title'), "required" => "required"));
                ?>
                <span class='text-left text-danger' id='title_err'></span>                     
                </div>                     
                <div class="form-group">
                <?php
                echo form_label("Contents (maximum 5000 words)", "content",array("class" => "control-label","style" => "color: black;"));
                echo form_textarea(array("name" => "content", "id" => "content", "class" => "form-control input-box", "rows" => "6", "value" => set_value('content'), "required" => "required"));
                ?>
                <span class='text-left text-danger' id='content_err'></span>    
                </div>
                <div class="form-group">                    
               <?php echo form_submit(array("name" => "save","id" => "save", "class" => "btn btn-info", "value" => "Save")) ?>
               <?php echo form_submit(array("name" => "cancel","id" => "cancel", "class" => "btn btn-warning", "type" => "reset", "value" => "Reset")) ?>
                </div>                    
                </div>
                </div>
                <?php echo form_close(); ?>  
                    </div>
                </div>
            </div>
            <?php
            if ($this->session->flashdata('hide_dashboard')):
            ?>
            <script>
               $(document).ready(function() {
                $('.dashboard').addClass("display");
                $('.theme').removeClass("display");    
                });      
            </script>
            <?php
              endif;
            ?> 
            
            
    <div class="col-md-10 theme">
        <div class="panel panel-default" style="border: none;">
            <div class="panel-body">
        <?php
        if ($this->session->flashdata('theme_select_msg')):
        echo "<div class='alert alert-success text-center'>";
        echo "<button type='button' class='close' data-dismiss='alert'>&times;</button>";
        echo $this->session->flashdata('theme_select_msg');
        echo "</div>";       
        endif;        
        ?>
    <div class="row">
        <div class="col-md-3">            
            <div class="thumbnail">
                <div class="caption">
                    <h4>clip one theme awesome</h4>
                    <p>short theme description</p>
                    <p><a href="" class="label label-danger" rel="tooltip" title="View this">Live Preview</a>
                    <a href="" class="label label-default" rel="tooltip" title="Select this">Select Theme</a></p>
                </div>
                <img src="<?php echo base_url('assets/login_page/images/download.jpg'); ?>" alt="...">
            </div>
      </div>
      
        <div class="col-md-3">            
            <div class="thumbnail">
                <div class="caption">
                    <h4>Thumbnail Headline</h4>
                    <p>short thumbnail description</p>
                    <p><a href="" class="label label-danger" rel="tooltip" title="View this">Live Preview</a>
                    <a href="" class="label label-default" rel="tooltip" title="Select this">Select Theme</a></p>
                </div>
                <img src="<?php echo base_url('assets/login_page/images/download (2).jpg'); ?>" alt="...">
            </div>
      </div>

        <div class="col-md-3">            
            <div class="thumbnail">
                <div class="caption">
                    <h4>Thumbnail Headline</h4>
                    <p>short thumbnail description</p>
                    <p><a href="" class="label label-danger" rel="tooltip" title="View this">Live Preview</a>
                    <a href="" class="label label-default" rel="tooltip" title="Select this">Select Theme</a></p>
                </div>
                <img src="<?php echo base_url('assets/login_page/images/download (3).jpg'); ?>" alt="...">
            </div>
      </div>

        <div class="col-md-3">            
            <div class="thumbnail">
                <div class="caption">
                    <h4>Thumbnail Headline</h4>
                    <p>short thumbnail description</p>
                    <p><a href="" class="label label-danger" rel="tooltip" title="View this">Live Preview</a>
                    <a href="" class="label label-default" rel="tooltip" title="Select this">Select Theme</a></p>
                </div>
                <img src="<?php echo base_url('assets/login_page/images/download (1).jpg'); ?>" alt="...">
            </div>
      </div>        
        
  </div>
                    </div>
                </div>
            </div>            
        </div>
    </div>
</div>