
		<!-- start: MAIN CONTAINER -->
                <div class="container" style="background-color: rgb(247, 247, 247);">
                                      <?php foreach ($user_blog as $row):?>                    
				<div class="container">
                                    <div class="page-header">
                                        <h1><a href="<?php echo site_url($uname)?>" class="blog_title"><?=$row->blog_title?></a></h1>
                                                <h3><?=$row->blog_sub_title?></h3>
                                    </div>                                    
				</div>
                                          <?php endforeach;?>                                      
                                    <div class="col-md-12">                                        
					<div class="row">                                                         
						<div class="col-md-8">
                                            <?php foreach ($user_blog_post as $post):?>                                                      
                                                    <div class="panel panel-default" style="border-color: #EDEDED;">
                                                    <div class="panel-body">                                                    
							<div class="col-md-9 col-md-offset-2">
							<h2>     
                                                            <a href="<?php echo site_url().$uname . '/blog_post/post_list/' .$post->id?>"><?=$post->blog_post_title?></a></h2>
                                                            
                                                            <p><?=$post->blog_post_content?>[...]</p>     
                                                    <?php if($this->uri->segment(4)!=""):?>
                                                            
                                                            <h2 style="color: blue;">Comments:</h2> <hr/>
                                                            
                                                        <?php if(@$commenter_info!=null && @$comment_list!=null):?>    
                                                        <?php foreach ($commenter_info as $cmntr_info):?>
                                                        <?php foreach ($comment_list as $cmnt):?>
                                                            
                                                            <div>
                                                                <h2><a href=""><?=$cmntr_info->user_first_name?> &nbsp; <?=$cmntr_info->user_last_name?></a><small>&nbsp;(at&nbsp;<?=$cmnt->date?>)</small></h2>
                                                                <div><?=$cmnt->comment?></div><br/>
                                                            </div>
                                                            <?php endforeach;?>
                                                            <?php endforeach;?>
                                                            <?php endif;?>
                                                            <?php endif;?>
                                                            
                                       <div id="comment_show"></div>
                                    <?php
                                    if($this->session->userdata('user_name') && $this->uri->segment(4)!=""):
                                    echo form_open_multipart('blog_post/comment', array("role" => "form", "id" => "commentForm", "class" => "cmxform"));
                                    $data = array('user_id'  => $uid);
                                    echo form_hidden($data);
                                    $data = array('post_id'  => $post_id);
                                    echo form_hidden($data);                                    
                                    echo form_label("Leave a Comment (maximum 1000 words)", "comment",array("class" => "control-label","style" => "color: black;"));
                                    echo form_textarea(array("name" => "comment", "id" => "comment", "rows" => "2", "class" => "form-control","value" => $this->session->userdata('comment'))) . "<br/>";
                                    echo form_submit(array("name" => "post","id" => "post", "class" => "btn btn-primary pull-right", "value" => "Post", "style" => "background-color: #000D34;border-color: #000D34;"));
                                    echo form_close();
                                    endif;
                                    ?>
                                    <span class='text-left text-danger' id='comment_err'></span>                        
                                                        </div>                                                                  
						</div>
                                        </div>                                                   
                                          <?php endforeach;?>                                                   
                                <div class="pull-right">
                                    <?php echo @$links; ?>
                                </div>
                                    </div>                                            
                                            
				<div class="col-md-3">
					<form>
					<div class="input-group">
					<input type="text" id="s" name="s" placeholder="Search..." class="form-control">
					</div>
                                        </form>
								<hr>
                                            <div class="panel panel-default" style="border-color: #EDEDED;">
                                               <div class="panel-body">                                                                 
								<h4>Recent Post</h4>
								<ul class="nav nav-list">
                                                        <?php foreach ($user_blog_post_list as $post):?>                                                                     
									<li>
									<a href="<?php echo site_url().$uname . '/blog_post/post_list/' .$post->id ?>"><?=$post->blog_post_title?></a>
									</li>
                                                            <?php endforeach;?>            
								</ul>
								<hr>
                                                                <h4>&COPY; Paco-BlogSpot.com 2014</h4>
                                               </div>
                                            </div>
						</div>
					</div>
                                        </div>
                                    </div>