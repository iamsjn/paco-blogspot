<?php
/*
 *description of registration page
 *author: Khairul Islam  
 */
class Registration extends CI_Controller {

    public function __construct() 
    {
        parent::__construct();
        $this->load->model('user/registration_model');
        $this->load->helper('form');        
        $this->load->helper('url');
        $this->load->library('session');
        $this->load->library('form_validation');        
    }

    function index() {
        
        $this->form_validation->set_rules('user_name', 'Username','trim|required|min_length[3]|is_unique[user_info.user_name]');
        $this->form_validation->set_rules('user_password', 'Password','trim|required|min_length[8]|matches[re_password]|callback_hashpassword');
        $this->form_validation->set_rules('re_password', 'Re-Password','trim|required|min_length[8]');        
        $this->form_validation->set_rules('user_email', 'Email','trim|required|min_length[8]|valid_email');       
        $this->form_validation->set_rules('user_gender', 'Gender','required');
        $this->form_validation->set_rules('agree', 'Agree with terms and condtions','required');

        if ($this->form_validation->run() === FALSE) {
            $this->session->set_userdata('sign_up', 'Sign up error!');
             $this->load->view('index');
        } 
        
        else {
                $data = array('user_name'=>$this->input->post('user_name'),'user_password'=>$this->input->post('user_password'),
                              'user_email'=>$this->input->post('user_email'),'user_gender'=>$this->input->post('user_gender'),"agree"=>$this->input->post('agree'));
                
                if (($this->registration_model->save_user_data($data))) {
                    $this->session->set_userdata('user_name', $data['user_name']);
                    redirect('index');
                } else {
                    $this->session->set_flashdata('submission_error', 'There was an error! Please try again!');
                    redirect('index');
                }
        }
   }    
    
    function sign_up() {
        
        $this->form_validation->set_rules('user_name', 'Username','trim|required|min_length[3]|is_unique[user_info.user_name]');
        $this->form_validation->set_rules('user_password', 'Password','trim|required|min_length[8]|matches[re_password]|callback_hashpassword');
        $this->form_validation->set_rules('re_password', 'Re-Password','trim|required|min_length[8]');        
        $this->form_validation->set_rules('user_email', 'Email','trim|required|min_length[8]|valid_email');       
        $this->form_validation->set_rules('user_gender', 'Gender','required');
        $this->form_validation->set_rules('agree', 'Agree with terms and condtions','required');

        if ($this->form_validation->run() === FALSE) {
            $data = array(
            'user_name' => form_error('user_name'),
            'user_password' => form_error('user_password'),
            're_password' => form_error('re_password'),
            'user_email' => form_error('user_email'),
            'user_gender' => form_error('user_gender'),                
            'agree' => form_error('agree')                    
            );
            echo json_encode($data);         
        } 
        
        else {
                $data = array('user_name'=>$this->input->post('user_name'),'user_password'=>$this->input->post('user_password'),
                              'user_email'=>$this->input->post('user_email'),'user_gender'=>$this->input->post('user_gender'),"agree"=>$this->input->post('agree'));
                
                if (($id=$this->registration_model->save_user_data($data))) {
                    
                    $this->session->set_userdata('user_name', $data['user_name']);
                    $this->session->set_userdata('user_id', $id);
                    $this->session->set_userdata('user_gender', $data['user_gender']);
                    $this->session->set_userdata('agree', $data['agree']);                    
                 
                    $data = array(
                        'redload' => 'refresh'                   
                    );
                    echo json_encode($data); 
                }
                else {
                    $this->session->set_flashdata('submission_error', 'There was an error! Please try again!');
                }
        }
   }
   
    function profile_update() {

        $this->form_validation->set_rules('fname', 'First Name','trim|required|min_length[3]');  
        $this->form_validation->set_rules('lname', 'Last Name','trim|required|min_length[3]');                  
        $this->form_validation->set_rules('email_1', 'Email','trim|required|min_length[8]|valid_email');       

        if ($this->form_validation->run() === FALSE) {
            $data = array(
            'fname_err' => form_error('fname'),
            'lname_err' => form_error('lname'),
            'email_err' => form_error('email_1')                                    
            );
            echo json_encode($data);         
        } 
        
        else {
                $data = array('user_first_name'=>$this->input->post('fname'),'user_last_name'=>$this->input->post('lname'),'user_email'=>$this->input->post('email_1'));

                $id = $this->session->userdata('user_id');                
                
                if ($this->registration_model->profile_update($data, $id)) {                                    

                    $this->session->set_userdata('user_fname', $data['user_first_name']);                            
                    $this->session->set_userdata('user_lname', $data['user_last_name']);
                    $this->session->set_userdata('user_email', $data['user_email']);
                    //$this->session->set_userdata('user_photo', $data['user_last_name']);                    
                    $this->session->set_flashdata('success_msg', 'Your changes successfully saved!');                    
                    $data = array('msg' => 'refresh');
                    echo json_encode($data); 
                }
                else {
                    $this->session->set_flashdata('submission_error', 'There was an error! Please try again!');
                }
        }
   }
   
  function update_profile_picture() {
            
                if ($id = $this->session->userdata('user_id')) {

                    if (isset($_FILES['userfile']['tmp_name'])) {
                        $upload_path = './assets/photos/user_profile_photo/' . $id;
                        if (!is_dir($upload_path)) {
                            mkdir($upload_path);
                        }
                        $config['upload_path'] = $upload_path;
                        $config['allowed_types'] = 'gif|jpg|png';
                        $config['max_size'] = '1024';
                        $config['file_name'] = "user_main_photo.jpg";
                        $config['overwrite'] = TRUE;
                        $this->load->library('upload', $config);
                        if ($this->upload->do_upload()) {
                            $upload_data = $this->upload->data();
                            $data['user_main_photo'] = '/assets/photos/user_profile_photo/' . $id . '/' . $upload_data['file_name'];
                            $path = '/assets/photos/user_profile_photo/' . $id . '/';

                            unset($config);
                            $this->load->library('image_lib');
                            $config['image_library'] = 'gd2';
                            $config['source_image'] = '.' . $data['user_main_photo'];
                            $config['create_thumb'] = TRUE;
                            $config['thumb_marker'] = '_profile';
                            $config['new_image'] = $upload_data['file_name'];
                            $config['maintain_ratio'] = FALSE;
                            $config['width'] = 175;
                            $config['height'] = 180;                            
                            $this->image_lib->initialize($config);
                            if (!$this->image_lib->resize()) {
                                $this->session->set_flashdata('form_success', $this->image_lib->display_errors());
                                $this->load->view('index');
                            } else {
                                $data['user_photo'] = $path . 'user_main_photo_profile.jpg';
                            }
                            
                            $this->registration_model->update_profile_photo($data, $id);
                            $this->session->set_userdata('user_photo', $data['user_photo']);                                               
                            $data = array('msg' => 'refresh');
                            echo json_encode($data);                            
                        }
                            else
                            {
                                $data = array('userfile_err' => 'Please select a photo.');
                                echo json_encode($data);                   
                            }                        
                    }

                }
  }   
   
    function password_change() {

        $this->form_validation->set_rules('password_1', 'Password','trim|required|min_length[8]|matches[re_password_1]|callback_hashpassword');  
        $this->form_validation->set_rules('re_password_1', 'Re-Password','trim|required|min_length[8]');                         

        if ($this->form_validation->run() === FALSE) {
            $data = array(
            'pass_1_err' => form_error('password_1'),
            'repass_1_err' => form_error('re_password_1')                                   
            );
            echo json_encode($data);         
        } 
        
        else {
                $data = array('user_password'=>$this->input->post('password_1'));

                $id = $this->session->userdata('user_id');                
                
                if ($this->registration_model->password_update($data, $id)) {
                    $this->session->set_flashdata('success_msg', 'Your changes successfully saved!');
                    $data = array('msg' => 'refresh');
                    echo json_encode($data);                    
                }
                else {
                    $this->session->set_flashdata('submission_error', 'There was an error! Please try again!');
                }
        }
   }    
   
    function check_user_name() {
                
        $this->form_validation->set_rules('user_name', 'User Name','trim|is_unique[user_info.user_name]');       

        if ($this->form_validation->run() === FALSE) {
            $data = array(
            'user_name' => 'User name is not available'                    
            );
            echo json_encode($data);         
        }
        else{
            $data = array(
            'user_name' => ''                    
            );
            echo json_encode($data);         
            }         
   }
   
   
 function hashpassword($password) {
        return md5($password);
    }   

}