<?php
/*
 *description of blog
 *author: Khairul Islam  
 */
class Blog extends CI_Controller {

    public function __construct() 
    {
        parent::__construct();
        $this->load->model('user/blog_model');
        $this->load->helper('form');        
        $this->load->helper('url');
        $this->load->library('session');
        $this->load->library('form_validation');        
    }  
    
    function index() {        
        $this->form_validation->set_rules('blog_title', 'Title','trim|required|min_length[10]|max_length[100]');
        $this->form_validation->set_rules('sub_title', 'Sub-Title','trim|required|min_length[5]|max_length[100]');

        if ($this->form_validation->run() === FALSE) {
            $data = array(
            'blog_title' => form_error('blog_title'),
            'sub_title' => form_error('sub_title')                   
            );
            echo json_encode($data);         
        } 
        
        else {
                $data = array('user_id'=>$this->session->userdata('user_id'),'blog_title'=>$this->input->post('blog_title'),'blog_sub_title'=>$this->input->post('sub_title'));
                
                if (($this->blog_model->create_blog($data))) {

                    $this->session->set_flashdata('hide_dashboard', 'Now get hide');                    
                    $this->session->set_flashdata('theme_select_msg', 'Now Select an Awesome Theme for Your Blog');
                    $this->session->set_userdata('blog_title', $data['blog_title']);                    
                    $this->session->set_userdata('sub_title', $data['blog_sub_title']);                     
                    $data = array(
                        'msg' => 'Now Select a Theme For Your Blog');
                    echo json_encode($data); 
                }
                else {
                    $this->session->set_flashdata('submission_error', 'There was an error! Please try again!');
                }
        }
   }
   
    function update() {        
        $this->form_validation->set_rules('blog_title', 'Title','trim|required|min_length[10]|max_length[100]');
        $this->form_validation->set_rules('sub_title', 'Sub-Title','trim|required|min_length[5]|max_length[100]');

        if ($this->form_validation->run() === FALSE) {
            $data = array(
            'blog_title' => form_error('blog_title'),
            'sub_title' => form_error('sub_title')                   
            );
            echo json_encode($data);         
        } 
        
        else {  
                $id = $this->session->userdata('user_id');
                $data = array('blog_title'=>$this->input->post('blog_title'),'blog_sub_title'=>$this->input->post('sub_title'));
                
                if (($this->blog_model->update_blog($data, $id))) {

                    $this->session->set_userdata('blog_title', $data['blog_title']);                    
                    $this->session->set_userdata('sub_title', $data['blog_sub_title']);
                    $this->session->set_flashdata('success_msg', 'Your changes successfully saved!');                    
                    $data = array('msg' => 'Now Select a Theme For Your Blog');
                    echo json_encode($data); 
                }
                else {
                    $this->session->set_flashdata('submission_error', 'There was an error! Please try again!');
                }
        }
   }     

}