<?php
/*
 *description of blog
 *author: Khairul Islam  
 */
class Blog_post extends CI_Controller {

    public function __construct() 
    {
        parent::__construct();
        $this->load->model('user/blog_post_model');
        $this->load->model('user/blog_model');
        $this->load->model('user/registration_model');        
        $this->load->helper('form');        
        $this->load->helper('url');
        $this->load->library('session');
        $this->load->library('form_validation');        
    }  
    
    function index() {        
        $this->form_validation->set_rules('title', 'Title','trim|required|min_length[3]|max_length[100]');
        $this->form_validation->set_rules('content', 'Content','trim|required|min_length[10]|max_length[5000]');

        if ($this->form_validation->run() === FALSE) {
            $data = array(
            'title' => form_error('title'),
            'content' => form_error('content')                   
            );
            echo json_encode($data);         
        } 
        
        else {
                $data = array('user_id'=>$this->session->userdata('user_id'),'blog_post_title'=>$this->input->post('title'),'blog_post_content'=>$this->input->post('content'));
                
                if (($this->blog_post_model->save_post($data))) {

                    $this->session->set_flashdata('success_msg', 'Your post successfully saved!');                                        
                    $data = array(
                        'msg' => 'reload');
                    echo json_encode($data); 
                }
                else {
                    $this->session->set_flashdata('submission_error', 'There was an error! Please try again!');
                }
        }
   }
   
    function comment() {        
        $this->form_validation->set_rules('comment', 'comment','trim|required|min_length[2]');

        if ($this->form_validation->run() === FALSE) {
            $data = array(
            'comment_err' => form_error('comment')                   
            );
            echo json_encode($data);         
        } 
        
        else {               
                $data = array('user_id'=>$this->input->post('user_id'),'post_id'=>$this->input->post('post_id'),'commenter_id'=>$this->session->userdata('user_id'),'comment'=>$this->input->post('comment'));
                if (($this->blog_post_model->save_comment($data))) {                                        
                    $data = array('comment_success' => $data['comment'], 'commenter_fname' => $this->session->userdata('user_fname'), 'commenter_lname' => $this->session->userdata('user_lname'));
                    echo json_encode($data); 
                }
                else {
                    $this->session->set_flashdata('submission_error', 'There was an error! Please try again!');
                }
            }
        } 
   
    function post_list()
    {
        $post_id = $this->uri->segment(4);
        $user_name = $this->uri->segment(1);
                if($user_name_result = $this->registration_model->check_user_name($user_name))
                    {
                        foreach($user_name_result as $row)
                           {  
                                $user_id = $row->id;                                                       
                           }
                        if($user_blog['user_blog'] = $this->blog_model->get_blog_data($user_id))
                        {   
                            $user_blog['uname'] = $user_name;
                            $user_blog['uid'] = $user_id;
                            $user_blog['post_id'] = $post_id;
                            if($user_blog['comment_list'] = $get_commenter_name = $this->blog_post_model->get_comment($user_id, $post_id))
                            {
                                foreach($get_commenter_name as $c_id)
                                    {  
                                        $cmntr_id = $c_id->commenter_id;                                                       
                                    }
                            $user_blog['commenter_info'] = $this->registration_model->get_commenter_info($cmntr_id);                                    
                            }
                            
                            $user_blog['user_blog_post_list'] = $this->blog_post_model->get_post($user_id);
                            $user_blog['user_blog_post'] = $this->blog_post_model->get_post_2($post_id); 
                            $this->load->view('user_theme/user_header');
                            $this->load->view('user_theme/userblog', $user_blog);
                            $this->load->view('user_footer');                            
                        }
                        else
                        {
                            $this->load->view('user_theme/404_error');
                        }
                    }         
    }      

}