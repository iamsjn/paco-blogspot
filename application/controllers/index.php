<?php
/*
 *description of login & logout
 *author: Khairul Islam  
 */
class Index extends CI_Controller {

    public function __construct() 
    {
        parent::__construct();
        $this->load->model('user/login_model');
        $this->load->model('user/blog_model');        
        $this->load->helper('url');
        $this->load->library('session');
        $this->load->library('form_validation');        
    }

    public function index()
	{
            $this->load->view('user_header');
            $this->load->view('index');      
            $this->load->view('user_footer');
	}
        
 function login()
 {
        $this->form_validation->set_rules('user_name_1', 'Username','trim|required|min_length[3]');
        $this->form_validation->set_rules('user_password_1', 'Password','trim|required|min_length[8]');

        if ($this->form_validation->run() === FALSE) {
                          
            $data = array('login_err' => 'Invalid Username and Password combination.');
            echo json_encode($data);         
        } 
        
        else {
                $user_name = $this->input->post('user_name_1');
                $user_password = $this->input->post('user_password_1');

                if (($result = $this->login_model->check_user_data($user_name, $user_password))) {
                        
                        foreach($result as $row)
                         {  
                            $user_id = $row->id;
                            $this->session->set_userdata('user_id', $row->id);                            
                            $this->session->set_userdata('user_name', $row->user_name);                          
                            $this->session->set_userdata('user_fname', $row->user_first_name);                            
                            $this->session->set_userdata('user_lname', $row->user_last_name);
                            $this->session->set_userdata('user_email', $row->user_email);
                            $this->session->set_userdata('user_photo', $row->user_photo);
                            $this->session->set_userdata('user_gender', $row->user_gender);
                            $this->session->set_userdata('agree', $row->agree);                             
                         }
                        
                        if($blog_result = $this->blog_model->get_blog_data($user_id))
                        {
                            foreach($blog_result as $blog)
                             {  
                                $this->session->set_userdata('blog_title', $blog->blog_title);                            
                                $this->session->set_userdata('sub_title', $blog->blog_sub_title);                           
                             }                         
                        }
                                                       
                        $data = array('login_success' => 'refresh');
                        echo json_encode($data); 
                }
                else 
                {
                    $data = array('login_err' => 'Invalid Username and Password combination.');
                    echo json_encode($data); 
                }
        }     
 }
 
 function logout()
 {
     $this->session->unset_userdata('user_id');
     $this->session->unset_userdata('user_name');
     $this->session->unset_userdata('user_fname');
     $this->session->unset_userdata('user_lname');
     $this->session->unset_userdata('user_photo');
     $this->session->unset_userdata('user_gender');
     $this->session->unset_userdata('agree');     
     $this->session->unset_userdata('blog_title');
     $this->session->unset_userdata('subtitle');
        $data = array('redload' => 'refresh');
        echo json_encode($data);      
 }

 function hashpassword($password) {
        return md5($password);
    }   
}