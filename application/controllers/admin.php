<?php
/*
 *description of admin home
 *author: Sujon Ahmed  
 */
class Admin extends CI_Controller {

    public function __construct() 
    {
        parent::__construct();        
        $this->load->helper('url');
        $this->load->library('session');
        $this->load->library('form_validation');        
    }
    
    public function index()
    {
   if($this->session->userdata('logged_in'))
   {
     $session_data = $this->session->userdata('logged_in');
     $data['username'] = $session_data['username'];
     $this->load->view('admin/header', $data);
     $this->load->view('admin/index', $data);
     $this->load->view('admin/footer');
     }
   else
   {
     redirect('admin', 'refresh');
   }
   }
 
 public function logout()
 {
   $this->session->unset_userdata('logged_in');
   $this->session->sess_destroy();
   redirect('admin', 'refresh');
 }
        
}