<?php
/*
 *description of userpanel
 *author: Khairul Islam  
 */
class Userblog extends CI_Controller {

    public function __construct() 
    {
        parent::__construct();
        $this->load->model('user/registration_model');
        $this->load->model('user/blog_model');
        $this->load->model('user/blog_post_model');
        $this->load->helper('url');
        $this->load->library('session');
        $this->load->library('form_validation');        
    }

    public function index()
	{   
            $user_name = $this->uri->segment(1);
                if($user_name_result = $this->registration_model->check_user_name($user_name))
                    {
                        foreach($user_name_result as $row)
                           {  
                                $user_id = $row->id;                                                       
                           }
                        if($user_blog['user_blog'] = $this->blog_model->get_blog_data($user_id))
                        {
                            $user_blog['uname'] = $user_name;
                            $user_blog['user_blog_post'] = $this->blog_post_model->get_post($user_id);
                            $user_blog['user_blog_post_list'] = $this->blog_post_model->get_post($user_id);
                            $this->load->view('user_theme/user_header');
                            $this->load->view('user_theme/userblog', $user_blog);
                            $this->load->view('user_footer');                            
                        }
                        else
                        {	$this->load->view('user_theme/user_header');
                            $this->load->view('user_theme/404_error');
                            $this->load->view('user_footer');  							
                        }
                    }                                       
	}
        
}