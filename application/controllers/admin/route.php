<?php
/*
 *description of admin home
 *author: Sujon Ahmed  
 */
class Index extends CI_Controller {

public function __construct() 
    {
        parent::__construct();
        $this->load->helper('file');
        $this->load->helper('url');
        $this->load->library('form_validation');        
    }

public function index()
    {
        $file = fopen("templates/index.html","r");
        $header = "";
        $body = "";
        $footer = "";
        while($header != '</head>')
        {
	$header = trim(fgets($file));
	$myfile = fopen("application/views/header.html", "a+") or die("Unable to open file!");
	fwrite($myfile, $header);	

	if($header == '</head>')
	{
	while($body != '<footer>')
	{
	$header = trim(fgets($file));
	$myfile = fopen("application/views/body.html", "a+") or die("Unable to open file!");
	fwrite($myfile, $header);	
	if($header == '<footer>')
	{	
	while($footer != '</html>')
	{
	$header = trim(fgets($file));
	$myfile = fopen("application/views/footer.html", "a+") or die("Unable to open file!");
	fwrite($myfile, $header);	
	if($header == '</html>')
	{	
            die;
        }
       }	
      }
     }
    }
   }
  }

    function createshoe() {
        $this->form_validation->set_rules('form[name]', 'Shoe Name', 'trim|required|min_length[2]|max_length[100]|xss_clean');
        $this->form_validation->set_rules('form[price]', 'Shoe Price', 'trim|required|min_length[2]|max_length[100]|numeric|xss_clean');
        if ($this->form_validation->run() === FALSE) {
            $this->session->set_flashdata('form_success', 'There was an error uploading the file, Please try again!');
            $this->view('admin/index');
        } else {
            if ($this->input->post('form')) {
                $form = $this->input->post('form');
                $data = array();
                foreach ($form as $key => $value) {
                    $data[$key] = $value;
                }
                if (($id = $this->men_shoe_model->save_men_shoe($data))) {
                    unset($data);
                    if (isset($_FILES['userfile']['tmp_name'])) {
                        $upload_path_1 = './templates/' . $id;
                        $upload_path = './application/views/' . $id;
                        
                        if (!is_dir($upload_path_1) && !is_dir($upload_path)) {
                            mkdir($upload_path_1);
                            mkdir($upload_path);
                        }
                        $config['upload_path'] = $upload_path_1;
                        $config['allowed_types'] = 'html';
                        $config['file_name'] = "main_image.jpg";
                        $config['overwrite'] = TRUE;
                        $this->load->library('upload', $config);
                        if ($this->upload->do_upload()) {
                            $upload_data = $this->upload->data();
                            $data['main_image'] = '/assets/img/men_collection/' . $id . '/' . $upload_data['file_name'];
                            $path = '/assets/img/men_collection/' . $id . '/';

                            unset($config);
                            $this->load->library('image_lib');
                            $config['image_library'] = 'gd2';
                            $config['source_image'] = '.' . $data['main_image'];
                            $config['create_thumb'] = TRUE;
							$config['thumb_marker'] = '_thumb';
                            $config['new_image'] = $upload_data['file_name'];
                            $config['maintain_ratio'] = TRUE;
                            $config['width'] = 256;
                            $config['height'] = 256;                            
                            $this->image_lib->initialize($config);
                            if (!$this->image_lib->resize()) {
                                $this->session->set_flashdata('form_success', $this->image_lib->display_errors());
                                redirect('admin/men_shoe');
                            } else {
                                $data['thumbnail_image'] = $path . 'main_image_thumb.jpg';
                            }

                            $this->image_lib->clear();
                            unset($config);
                            $config['image_library'] = 'gd2';
                            $config['source_image'] = '.' . $data['main_image'];
                            $config['thumb_marker'] = '_gallery';
                            $config['new_image'] = $upload_data['file_name'];
                            $config['maintain_ratio'] = TRUE;
                            $config['width'] = 512;
                            $config['height'] = 512;
                            $this->image_lib->initialize($config);
                            if (!$this->image_lib->resize()) {
                                $this->session->set_flashdata('form_success', $this->image_lib->display_errors());
                                redirect('admin/men_shoe');
                            } else {
                                $data['gallery_image'] = $path . 'main_image_gallery.jpg';
                            }
                            $this->men_shoe_model->save_men_shoe($data, $id);
                        }
                    }

                    $this->session->set_flashdata('form_success', 'The shoe has been saved successfully');
                    redirect('admin/men_shoe');
                } else {
                    $this->session->set_flashdata('form_success', 'There was an error saving the shoe, please try again');
                    redirect('admin/men_shoe');
                }
            }
        }
    }  
  
}